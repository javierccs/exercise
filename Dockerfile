FROM ubuntu:latest

ADD ansible/playbooks/ /etc/ansible/playbooks/
ADD ansible/files/ /etc/ansible/files/
ADD ansible/hosts /etc/ansible/hosts

WORKDIR /etc/ansible

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -q -y --no-install-recommends install python-yaml \
               python-jinja2 python-httplib2 python-keyczar \
               python-paramiko python-setuptools \
               python-pkg-resources python-pip
RUN mkdir -p /etc/ansible/
RUN pip install ansible
RUN ansible-playbook /etc/ansible/playbooks/web.yml -c local

EXPOSE 22 80

CMD /usr/sbin/apache2ctl -D FOREGROUND

#RUN echo "#!/bin/bash\n/etc/init.d/apache2 start\n/bin/bash" > /root/init.sh && \
    #chmod 755 /root/init.sh

#ENTRYPOINT ["/root/init.sh"]
