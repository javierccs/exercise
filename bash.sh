#!/bin/bash
echo "Building the image"
docker build -t web .
echo "Running the image built"
docker run --rm -d --name web_server -p 8080:80 web 
